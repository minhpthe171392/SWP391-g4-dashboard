<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="java.util.ArrayList" %>
<%@page import="model.Medicine" %>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <link rel="apple-touch-icon" sizes="76x76" href="assets/img/apple-icon.png">
        <link rel="icon" type="image/png" href="assets/img/favicon.png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>
            Medicine Management
        </title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
        <!--     Fonts and icons     -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700,200" rel="stylesheet" />
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <!-- CSS Files -->
        <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="assets/css/paper-dashboard.css?v=2.0.1" rel="stylesheet" />
        <!-- CSS Just for demo purpose, don't include it in your project -->
        <link href="assets/demo/demo.css" rel="stylesheet" />
        <style>
            #popup-background{
                display: none;
                width:100%;
                height:100%;
                position: fixed;
                z-index: 5000;
                background-color:rgba(120, 120, 120, 0.8);
            }
            .table button{
                padding: 10px 20px;
                font-size: 15px;
                background: #51cbce;
                color: white;
                border: none;
                outline: none;
                cursor: pointer;
                border-radius: 10px;
            }
            .popup{
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%,-50%);
                width:500px;
                padding: 0px 40px;
                background: #FFFFFF;
                box-shadow: 2px 2px 5px 5px rgba(0,0,0,0.15);
                border-radius: 10px;
            }
            .popup .close-btn{
                position: absolute;
                top: 10px;
                right:10px;
                width:15px;
                height:15px;
                background:#888;
                color:#eee;
                text-align: center;
                line-height: 15px;
                border-radius: 15px;
                cursor: pointer;
            }
            .popup .form h2{
                text-align: center;
                color: #222;
                margin: 10px auto;
                font-size: 20px;
            }
            .popup .form .form-element{
                text-align: left;
                margin: 15px 0px;
            }
            .popup .form .form-element label{
                font-size:14px;
                color:#222;
            }
            .popup .form .form-element input{
                margin-top: 5px;
                display: block;
                width:100%;
                padding:10px;
                outline:none;
                border:1px solid #aaa;
                border-radius:5px;
            }
            .popup .form .form-element button{
                width:100%;
                height: 40px;
                border:none;
                outline:none;
                font-size:15px;
                background: #51cbce;
                color: #F5F5F5;
                border-radius: 10px;
                cursor: pointer;
            }
        </style>
    </head>

    <body class="">
        <div class="wrapper ">
            <div id="popup-background">
                <div class="popup">
                    <div class="close-btn">&times;</div>
                    <div class="form">
                        <form action="medicine?option=1" method="post">
                            <h2>Update Medicine</h2>
                            <div class="form-element">
                                <label for="name">Name</label>
                                <input type="text" name="name">
                            </div>
                            <div class="form-element">
                                <label for="unit">Unit</label><br>
                                <select>
                                    <option value="1">Option1</option>
                                    <option value="2">Option2</option>
                                    <option value="3">Option3</option>
                                    <option value="4">Option4</option>
                                </select>
                            </div>
                            <div class="form-element">
                                <label for="quantity">Quantity</label>
                                <input type=number name="quantity">
                            </div>
                            <div class="form-element">
                                <label for="price">Price</label>
                                <input type=number name="price">
                            </div>
                            <div class="form-element">
                                <button type="submit">Update</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="sidebar" data-color="white" data-active-color="danger">
                <div class="logo">
                    <a href="https://www.creative-tim.com" class="simple-text logo-mini">
                        <div class="logo-image-small">
                            <img src="assets/img/logo-small.png">
                        </div>
                        <!-- <p>CT</p> -->
                    </a>
                    <a href="https://www.creative-tim.com" class="simple-text logo-normal">
                        Creative Tim
                        <!-- <div class="logo-image-big">
                          <img src="assets/img/logo-big.png">
                        </div> -->
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li>
                            <a href="./dashboard.jsp">
                                <i class="nc-icon nc-bank"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="./icons.jsp">
                                <i class="nc-icon nc-diamond"></i>
                                <p>Icons</p>
                            </a>
                        </li>
                        <li>
                            <a href="./map.jsp">
                                <i class="nc-icon nc-pin-3"></i>
                                <p>Maps</p>
                            </a>
                        </li>
                        <li>
                            <a href="./notifications.jsp">
                                <i class="nc-icon nc-bell-55"></i>
                                <p>Notifications</p>
                            </a>
                        </li>
                        <li>
                            <a href="./user.jsp">
                                <i class="nc-icon nc-single-02"></i>
                                <p>User Profile</p>
                            </a>
                        </li>
                        <li class="active ">
                            <a href="./medicine.jsp">
                                <i class="nc-icon nc-tile-56"></i>
                                <p>Table List</p>
                            </a>
                        </li>
                        <li>
                            <a href="./typography.jsp">
                                <i class="nc-icon nc-caps-small"></i>
                                <p>Typography</p>
                            </a>
                        </li>
                        <li class="active-pro">
                            <a href="./upgrade.jsp">
                                <i class="nc-icon nc-spaceship"></i>
                                <p>Upgrade to PRO</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-panel">

                <!-- Navbar -->
                <nav class="navbar navbar-expand-lg navbar-absolute fixed-top navbar-transparent">
                    <div class="container-fluid">
                        <div class="navbar-wrapper">
                            <div class="navbar-toggle">
                                <button type="button" class="navbar-toggler">
                                    <span class="navbar-toggler-bar bar1"></span>
                                    <span class="navbar-toggler-bar bar2"></span>
                                    <span class="navbar-toggler-bar bar3"></span>
                                </button>
                            </div>
                            <a class="navbar-brand" href="javascript:;">Paper Dashboard 2</a>
                        </div>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navigation" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                            <span class="navbar-toggler-bar navbar-kebab"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-end" id="navigation">
                            <form>
                                <div class="input-group no-border">
                                    <input type="text" value="" class="form-control" placeholder="Search...">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            <i class="nc-icon nc-zoom-split"></i>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <ul class="navbar-nav">
                                <li class="nav-item">
                                    <a class="nav-link btn-magnify" href="javascript:;">
                                        <i class="nc-icon nc-layout-11"></i>
                                        <p>
                                            <span class="d-lg-none d-md-block">Stats</span>
                                        </p>
                                    </a>
                                </li>
                                <li class="nav-item btn-rotate dropdown">
                                    <a class="nav-link dropdown-toggle" href="http://example.com" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="nc-icon nc-bell-55"></i>
                                        <p>
                                            <span class="d-lg-none d-md-block">Some Actions</span>
                                        </p>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link btn-rotate" href="javascript:;">
                                        <i class="nc-icon nc-settings-gear-65"></i>
                                        <p>
                                            <span class="d-lg-none d-md-block">Account</span>
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
                <!-- End Navbar -->
                <div class="content">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-plain">
                                <div class="card-header">
                                    <h4 class="card-title"> Medicine List</h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead class=" text-primary">
                                            <th>Name</th>
                                            <th>Quantity</th>
                                            <th>Unit</th>
                                            <th>Price</th>
                                            <th></th>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${medList}" var="med">
                                                    <tr>
                                                        <td>${med.getName()}</td>
                                                        <td>${med.getQuantity()}</td>
                                                        <td>${med.getUnit()}</td>
                                                        <td>${med.getPrice()}vnd</td>
                                                        <td class="text-right">
                                                            <button id="show-update" value="${med.getId()}" 
                                                                    onclick="show()">Update</button>
                                                            <button><a href="medicine?option=2?id=${med.getId()}" method="post" style="color: white">Delete</a></button>
                                                        </td>
                                                    </tr>
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <footer class="footer footer-black  footer-white ">
                    <div class="container-fluid">
                        <div class="row">
                            <nav class="footer-nav">
                                <ul>
                                    <li><a href="https://www.creative-tim.com" target="_blank">Creative Tim</a></li>
                                    <li><a href="https://www.creative-tim.com/blog" target="_blank">Blog</a></li>
                                    <li><a href="https://www.creative-tim.com/license" target="_blank">Licenses</a></li>
                                </ul>
                            </nav>
                            <div class="credits ml-auto">
                                <span class="copyright">
                                    <script>
                                        document.write(new Date().getFullYear());
                                    </script>, made with <i class="fa fa-heart heart"></i> by Creative Tim
                                </span>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
        <script>
            function show() {
                let medicineID = document.getElementById("show-update").value;
                alert(medicineID);
                document.getElementById("popup-background").style.display = 'block';
            }
            ;
            document.querySelector(".popup .close-btn")
                    .addEventListener("click", function hide() {
                        document.getElementById("popup-background").style.display = 'none';
                    });
        </script>
        <!--   Core JS Files   -->
        <script src="assets/js/core/jquery.min.js"></script>
        <script src="assets/js/core/popper.min.js"></script>
        <script src="assets/js/core/bootstrap.min.js"></script>
        <script src="assets/js/plugins/perfect-scrollbar.jquery.min.js"></script>
        <!--  Google Maps Plugin    -->
        <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script>
        <!-- Chart JS -->
        <script src="assets/js/plugins/chartjs.min.js"></script>
        <!--  Notifications Plugin    -->
        <script src="assets/js/plugins/bootstrap-notify.js"></script>
        <!-- Control Center for Now Ui Dashboard: parallax effects, scripts for the example pages etc -->
        <script src="assets/js/paper-dashboard.min.js?v=2.0.1" type="text/javascript"></script><!-- Paper Dashboard DEMO methods, don't include it in your project! -->
        <script src="assets/demo/demo.js"></script>
    </body>

</html>