/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao.medicineDAO;

import dbContext.DBContext;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.Medicine;

/**
 *
 * @author Admin
 */
public class MedicineDAO extends DBContext {

    Connection cnn;
    PreparedStatement pstm;
    ResultSet rs;

    public void addMedicine(Medicine med) {
        try {
            String strAdd = "insert into medicine(medicine_name,medicine_unit,medicine_price,medicine_quantity) values (?,?,?,?)";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strAdd);
            pstm.setString(1, med.getName());
            pstm.setString(2, med.getUnit());
            pstm.setDouble(3, med.getPrice());
            pstm.setInt(4, med.getQuantity());
            pstm.execute();
        } catch (Exception e) {
            System.out.println("MedicineAdd: " + e.getMessage());
        }
    }

    public void updateMedicine(Medicine med) {
        try {
            String strUpdate = "update medicine set medicine_price= ?, medicine_quantity=? where medicine_id=?";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strUpdate);
            pstm.setDouble(1, med.getPrice());
            pstm.setInt(2, med.getQuantity());
            pstm.setInt(3, Integer.parseInt(med.getId()));
            pstm.execute();
        } catch (Exception e) {
            System.out.println("MedicineUpdate: " + e.getMessage());
        }
    }

    public void deleteMedicine(Medicine med) {
        try {
            String strDelete = "delete from medicine where medicine_id=?";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strDelete);
            pstm.setInt(1, Integer.parseInt(med.getId()));
            pstm.execute();
        } catch (Exception e) {
            System.out.println("MedicineDelete: " + e.getMessage());
        }
    }

    public List<Medicine> getListMedicine() {
        List<Medicine> medList = new ArrayList<>();
        try {
            System.out.println("connect!");
            String strSelect = "select medicine_id, medicine_name, "
                    + "medicine_unit, medicine_quantity, medicine_price "
                    + "from medicine";
            cnn = new DBContext().connection;
            pstm = cnn.prepareStatement(strSelect);
            rs = pstm.executeQuery();
            while(rs.next()){
                medList.add(new Medicine(rs.getString(1),rs.getString(2),
                        rs.getString(3),Integer.parseInt(rs.getString(4)), 
                        Double.parseDouble(rs.getString(5))));
            }
            System.out.println(medList);
        } catch (Exception e) {
            System.out.println("MedicineList: " + e.getMessage());
        }
        return medList;
    }

}
